import os
import subprocess
import threading
from string import printable
from subprocess import PIPE, Popen

from kivy.clock import Clock
from kivy.lang import Builder
from kivy.uix.boxlayout import BoxLayout

Builder.load_file("ui/training.kv")


class Training(BoxLayout):
  process = None
  params = {
      "dataset_path": "~/dataset/Casia-Webface",
      "learning_rate": 1e-2,
      "tar": .5,
      "epoch_num": 50,
      "batch_size": 128,
  }
  __loading_msg = "Mohon tunggu...\n\n"

  def __init__(self, **kwargs):
    super(Training, self).__init__(**kwargs)
    Clock.schedule_once(self.init_ui, 0)

  def init_ui(self, dt=0):
    # setup filters
    self.ids.epoch_num.ids.input.input_filter = self.__filter_number
    self.ids.batch_size.ids.input.input_filter = self.__filter_number
    self.ids.learning_rate.ids.input.input_filter = self.__filter_number
    self.ids.tar.ids.input.input_filter = self.__filter_number

    for param in self.params:
      # set initial value
      self.ids[param].ids.input.text = str(self.params[param])

      # input binding
      self.ids[param].ids.input.bind(text=self.__on_set_param)

  def evaluate(self):
    self.start_process("evaluate")
    command = ["python", "evaluate.py"]
    threading.Thread(target=self.execute_command, args=(command,)).start()

  def train(self):
    self.start_process("train")
    command = ["python", "train.py"]
    threading.Thread(target=self.execute_command, args=(command,)).start()

  def execute_command(self, command):
    p = Popen(command, stdout=PIPE, universal_newlines=True, preexec_fn=os.setsid)

    for stdout_line in iter(p.stdout.readline, ""):
      if self.process is None:
        break

      if self.process == "train":
        self.set_output(stdout_line)
      else:
        self.print_output(stdout_line)

    p.stdout.close()
    p.kill()

    if self.process != None:
      self.print_output("\nSelesai")

    self.finish_process()

  def start_process(self, name):
    self.ids.btn_evaluate.hide = True
    self.ids.btn_train.hide = True
    self.ids.btn_stop.hide = False

    self.set_output(self.__loading_msg)
    self.process = name

  def finish_process(self):
    if self.process != None:
      self.ids.btn_evaluate.hide = False
      self.ids.btn_train.hide = False
      self.ids.btn_stop.hide = True

      print("'%s' process ended" % self.process)
      self.process = None

  def stop_process(self):
    self.finish_process()

    if self.ids.output.text == self.__loading_msg:
      self.set_output()

  def print_output(self, output, max_len=2048):
    current_len = len(self.ids.output.text)
    output_len = len(output)
    next_output_len = current_len + output_len

    if (next_output_len >= max_len):
      next_output = self.ids.output.text + output
      self.ids.output.text = next_output[(next_output_len - max_len):]
    else:
      self.ids.output.text += output

  def set_output(self, output=""):
    self.ids.output.text = ''.join(filter(lambda x: x in printable, output))

  def __filter_number(self, value, undo):
    return value if str.isdigit(value) else False

  def __on_set_param(self, instance, value):
    self.params[instance.name] = value
