import random
import cv2
import numpy as np
from kivy.app import App
from kivy.clock import Clock
from kivy.graphics.texture import Texture
from kivy.lang import Builder
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.popup import Popup
from kivy.properties import ObjectProperty

from .components.photo import Photo
from app.augmentation import Augmentation

Builder.load_file("ui/face_verification.kv")


class FaceVerification(FloatLayout):
  TAR = 0.5
  model = None

  def __init__(self, **kwargs):
    super(FaceVerification, self).__init__(**kwargs)
    Clock.schedule_once(self.init_ui, 0)
    self.augmentation = Augmentation()
    self.input_shape = (31, 39, 1)

  def init_ui(self, dt=0):
    self.ids.photo_1.bind(image_set=self.set_image)
    self.ids.photo_2.bind(image_set=self.set_image)

  def set_image(self, instance, is_set):
    if is_set:
      # enable photo 2 if photo 1 is set
      self.ids.photo_2.enable = self.ids.photo_1.image_set

      # verify face if both images are applied
      if self.ids.photo_1.image_set and self.ids.photo_2.image_set:
        self.verify(self.ids.photo_1.image, self.ids.photo_2.image)

  def verify(self, image_left, image_right):
    # image grayscale
    image_left = self.augmentation.grayscale(image_left)
    image_right = self.augmentation.grayscale(image_right)

    # image resize & reshape to input model
    img_size = self.input_shape[:2]
    image_left = self.augmentation.resize(image_left, img_size).reshape(self.input_shape)
    image_right = self.augmentation.resize(image_right, img_size).reshape(self.input_shape)

    # image normalization
    image_left = self.augmentation.normalize(image_left)
    image_right = self.augmentation.normalize(image_right)

    result = self.model.predict([np.array([image_left]), np.array([image_right])])

    self.display_result(result[0][0].item())

  def display_result(self, distance):
    self.ids.btn_reset.hide = False
    self.ids.result.hide = False
    self.ids.result.is_valid = distance < self.TAR
    self.ids.result.distance = distance

  def reset(self):
    self.ids.result.hide = True
    self.ids.btn_reset.hide = True
    self.ids.photo_2.enable = False

    # reset images
    self.ids.photo_1.reset()
    self.ids.photo_2.reset()
