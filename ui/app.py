from kivy.app import App
from kivy.lang import Builder
from kivy.uix.tabbedpanel import TabbedPanel, TabbedPanelItem
from kivy.uix.widget import WidgetBase

from .face_verification import FaceVerification
from .training import Training

Builder.load_file("ui/components/styles.kv")
Builder.load_file("ui/components/form_input.kv")
Builder.load_file("ui/components/header.kv")
root = Builder.load_file("ui/app.kv")

class MainApp(App):
  def __init__(self, model, **kwargs):
    super(MainApp, self).__init__(**kwargs)
    self.model = model

  # TODO: set min windows size
  def build(self):
    root.ids.face_verification.model = self.model
    root.ids.training.model = self.model

    return root
