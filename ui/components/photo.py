import os
import traceback

import cv2
import numpy as np
from kivy.clock import Clock
from kivy.graphics.texture import Texture
from kivy.lang import Builder
from kivy.properties import BooleanProperty
from kivy.uix.image import Image
from kivy.uix.stacklayout import StackLayout

from .file_browser import FileBrowser
from .message_box import MessageBox

Builder.load_file("ui/components/photo.kv")


class Photo(StackLayout):
  file_filters = ["*.jpg", "*.jpeg", "*.png", "*.bmp", "*.webp"]
  image_set = BooleanProperty(False)
  image = None

  def __init__(self, **kwargs):
    super(Photo, self).__init__(**kwargs)
    Clock.schedule_once(self.init_ui, 0)

  def init_ui(self, dt=0):
    self.ids.btn_file.bind(value=self.select_image)

  def start_camera(self):
    self.ids.btn_camera.hide = True
    self.ids.btn_file.hide = True
    self.ids.btn_camera_snap.hide = False
    self.ids.btn_camera_cancel.hide = False

    self.capture = cv2.VideoCapture(0)
    self.camera_schedule = Clock.schedule_interval(self.render_camera, 1.0 / 15.0)

  def stop_camera(self):
    self.ids.btn_camera.hide = False
    self.ids.btn_file.hide = False
    self.ids.btn_camera_snap.hide = True
    self.ids.btn_camera_cancel.hide = True

    self.camera_schedule.cancel()
    self.capture.release()

  def cancel_camera(self):
    self.stop_camera()
    self.revert_back()

  def revert_back(self):
    if not self.image_set:
      self.reset()
    else:
      self.display_image(self.image)

  def select_image(self, instance, img_file):
    try:
      filename, ext = os.path.splitext(img_file)

      if "*" + ext in self.file_filters:
        image = cv2.imread(img_file)
        self.set_image(image)

        image = self.__fit_image(image, self.ids.photo.size, crop=False)
        self.display_image(image)
      else:
        raise Exception("Invalid Image")
    except Exception as e:
      message = "File gambar tidak valid."
      MessageBox(title="Warning", message=message).open()
      traceback.print_exc()

  def take_photo(self):
    ret, image = self.capture.read()
    self.set_image(image)

    self.stop_camera()
    self.display_image(self.image)

  def reset(self):
    self.display_image("images/face-icon.png")
    self.set_image()

  def set_image(self, image=None):
    image_exists = not image is None

    if image_exists:
      self.image = image

      # reset image_set to force trigger binding event on face_verification.py
      self.image_set = False

    self.image_set = image_exists

  def render_camera(self, dt):
    ret, image = self.capture.read()

    if ret:
      image = self.__fit_image(image, self.ids.photo.size)
      self.display_image(image)

  def display_image(self, image):
    if isinstance(image, str):
      texture = Image(source=image).texture
      texture.wrap = "clamp_to_edge"
      self.ids.photo.texture = texture
    else:
      image = cv2.flip(image, 0)
      self.ids.photo.texture = self.__make_texture(image)

  def __fit_image(self, image, shape, crop=True):
    [fr_width, fr_height] = shape
    fr_width, fr_height = (round(fr_width), round(fr_height))
    im_width, im_height = (image.shape[1], image.shape[0])

    # resize with aspect ratio
    if (fr_height / fr_width) > (im_height / im_width):
      # resize to height
      scale = fr_height / im_height
      image = cv2.resize(image, (round(scale * im_width), fr_height))

      # crop to frame size
      if crop:
        margin = round((image.shape[1] - fr_width) / 2)
        image = image[:, margin:(fr_width + margin)]
    else:
      # resize to width
      scale = fr_width / im_width
      image = cv2.resize(image, (fr_width, round(scale * im_height)))

      # crop to frame size
      if crop:
        margin = round((image.shape[0] - fr_height) / 2)
        image = image[margin:(fr_height + margin), :]

    return image

  def __make_texture(self, image, colorfmt='bgr'):
    texture = Texture.create(size=(image.shape[1], image.shape[0]), colorfmt=colorfmt)
    texture.blit_buffer(image.tostring(), colorfmt=colorfmt, bufferfmt='ubyte')
    texture.wrap = "clamp_to_edge"

    return texture
