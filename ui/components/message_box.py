import os

import cv2
from kivy.clock import Clock
from kivy.graphics.texture import Texture
from kivy.lang import Builder
from kivy.properties import ObjectProperty
from kivy.uix.button import Button
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.image import Image
from kivy.uix.popup import Popup

Builder.load_file("ui/components/message_box.kv")


class MessageBox(Popup):
  title = ObjectProperty(None)
  message = ObjectProperty(None)
