import os

import cv2
from kivy.clock import Clock
from kivy.graphics.texture import Texture
from kivy.lang import Builder
from kivy.properties import ListProperty, ObjectProperty, StringProperty
from kivy.uix.button import Button
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.image import Image
from kivy.uix.popup import Popup

Builder.load_file("ui/components/file_browser.kv")


class FileBrowser(Popup):
  select = ObjectProperty(None)
  cancel = ObjectProperty(None)
  filters = ObjectProperty(None)


class FileBrowserBtn(Button):
  value = StringProperty(None)
  filters = ListProperty(["*.*"])
  popup = ObjectProperty(None)

  def show_dialog(self):
    if self.popup is None:
      self.popup = FileBrowser(select=self.select, filters=self.filters)

    self.popup.open()

  def select(self, dirname, filepath):
    self.value = filepath
    self.popup.dismiss()
