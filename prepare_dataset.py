from app import SiameseModel, Dataset

images_path = "/home/ramdhan/dev/dataset/casia-align-182"
tfrecord_files = "./dataset/data-*.tfrecord"

if __name__ == "__main__":
  record_files = "./dataset/data-%06d.tfrecord"
  Dataset(images_path).prepare(record_files, im_size=(72, 72))
  print("Finished preparing dataset")
