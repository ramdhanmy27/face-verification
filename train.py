import os
import tensorflow as tf
from app import SiameseModel, Dataset

# os.environ['CUDA_VISIBLE_DEVICES'] = '-1'  # force CPU
images_path = "/home/ramdhan/dev/dataset/casia-align-182"
input_shape = (31, 39, 1)
batch_size = 128
learning_rate = 1e-5
epochs = 10

model_file = "model/deepid-model.json"
weights_file = "model/deepid-weights.h5"

if __name__ == "__main__":
  tfrecord_files = "./dataset/data-*.tfrecord"
  # tfrecord_files = "./dataset/data-00000*.tfrecord"
  train_ds, val_ds, test_ds = Dataset(images_path).from_tfrecords(
      tfrecord_files, im_resize=input_shape[:2], grayscale=True)

  siameseModel = SiameseModel(input_shape,
                              model_file=model_file,
                              weights_file=weights_file)
  model = siameseModel.train(train_ds, val_ds,
                             overwrite_model=False,
                             batch_size=batch_size,
                             learning_rate=learning_rate,
                             epochs=epochs)
  model.evaluate(test_ds.batch(batch_size))
