# Face Verification

## Installation

```
pip install -r requirements.txt
```

## Preparing Dataset
split dataset (train.csv, test.csv, val.csv), generate pairs

```
python prepare_dataset.py
```

## Training

```
python train.py
```

## Evaluating

```
python evaluate.py
```

## How to Run GUI

```
python main.py
```