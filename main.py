from kivy.app import App
from kivy.uix.tabbedpanel import TabbedPanel

from app import SiameseModel
from ui.app import MainApp

if __name__ == '__main__':
  model_file = "model/deepid-model.json"
  weights_file = "model/deepid-weights.h5"
  model = SiameseModel(model_file=model_file, weights_file=weights_file).get_model()

  MainApp(model).run()
