import random

import matplotlib.pyplot as plt


def show_images(images, grayscale=False):
  fig = plt.figure()
  total_img = len(images)
  i = 1

  for image in images:
    fig.add_subplot(1, total_img, i)
    cmap = "gray" if grayscale else None
    plt.imshow(image, cmap=cmap)
    i += 1

  plt.show()
