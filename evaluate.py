import os
import tensorflow as tf
from app import SiameseModel, Dataset
import cv2

images_path = "/home/ramdhan/dev/dataset/casia-align-182"
input_shape = (31, 39, 1)
batch_size = 128
learning_rate = 1e-5
epochs = 10

model_file = "model/deepid-model.json"
weights_file = "model/deepid-weights.h5"

if __name__ == "__main__":
  tfrecord_files = "./dataset/data-*.tfrecord"
  train_ds, val_ds, test_ds = Dataset(images_path).from_tfrecords(
      tfrecord_files, im_resize=input_shape[:2], grayscale=True)

  model = SiameseModel(input_shape,
                       model_file=model_file,
                       weights_file=weights_file).get_model()
  # model.evaluate(test_ds.batch(batch_size))

  # Predict
  labels = []
  predictions = {}

  for pair, similarity in test_ds.batch(1):
    labels.append(similarity)
    result = model.predict(pair)

    for i in range(1, 11):
      TAR = i/10

      if TAR not in predictions:
        predictions[TAR] = []

      predictions[TAR].append(1 if result < TAR else 0)

  # Calculate accurations
  print("Total data: ", len(labels))

  for i in range(1, 11):
    TAR = i/10

    conf_mat = tf.math.confusion_matrix(labels, predictions[TAR])
    TP = conf_mat[1][1]
    TN = conf_mat[0][0]
    FP = conf_mat[0][1]
    FN = conf_mat[1][0]

    print("TAR: ", TAR)
    print("TP: ", TP)
    print("TN: ", TN)
    print("FP: ", FP)
    print("FN: ", FN)
    print("acc: ", (((TP + TN)/(TP + TN + FP + FN))*100))
    print("pre: ", ((TP/(TP +FP))*100))
    print("rec: ", ((TP/(TP + FN))*100))
    print()
