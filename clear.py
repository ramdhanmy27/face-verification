from os import path

model_file = "model.json"
weights_file = "weights.h5"

if path.exists(model_file):
    os.remove(model_file)
    print("'%s' is removed" % model_file)

if path.exists(weights_file):
    os.remove(weights_file)
    print("'%s' is removed" % weights_file)
