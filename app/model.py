from datetime import datetime
from os import path

import numpy as np
import tensorflow as tf
from tensorflow.keras import backend as K
from tensorflow.keras import layers, models, optimizers, callbacks

from .architectures import simple, deepid, resnet_v1
from .augmentation import Augmentation


class SiameseModel:
  # TODO: determine default model input shape
  def __init__(self, input_shape=(72, 72, 1), model_file="model.json", weights_file="weights.h5"):
    self.model_file = model_file
    self.weights_file = weights_file
    self.input_shape = input_shape

  def euclidean_distance(self, vects):
    x, y = vects
    sum_square = K.sum(K.square(x - y), axis=1, keepdims=True)
    return K.sqrt(K.maximum(sum_square, K.epsilon()))

  def eucl_dist_output_shape(self, shapes):
    shape1, shape2 = shapes
    return (shape1[0], 1)

  def contrastive_loss(self, y_true, y_pred):
    margin = 1
    y_true = tf.cast(y_true, "float32")
    margin_square = K.square(K.maximum(margin - y_pred, 0))

    return K.mean(((1 - y_true) * margin_square) + (y_true * K.square(y_pred)))

  def accuracy(self, y_true, y_pred):
    return K.mean(K.equal(y_true, K.cast(y_pred < 0.5, y_true.dtype)))

  def create_base_network(self, input_shape):
    inputs = layers.Input(shape=input_shape)
    x = deepid(inputs)
    # x = simple(inputs)
    # x = resnet_v1(inputs)

    return models.Model(inputs, x)

  def get_model(self, load_weights=True, learning_rate=1e-3):
    if path.exists(self.model_file):
      # load model json if exists
      with open(self.model_file, 'r') as f:
        model = models.model_from_json(f.read(), custom_objects={
            "euclidean_distance": self.euclidean_distance,
            "eucl_dist_output_shape": self.eucl_dist_output_shape,
        })
        print("# Loaded model from disk")
    else:
      # build model
      base_network = self.create_base_network(self.input_shape)
      input_left = layers.Input(shape=self.input_shape, name="input_left")
      input_right = layers.Input(shape=self.input_shape, name="input_right")

      processed_a = base_network(input_left)
      processed_b = base_network(input_right)

      distance = layers.Lambda(self.euclidean_distance,
                               output_shape=self.eucl_dist_output_shape,
                               name="euclidean_distance")([processed_a, processed_b])
      model = models.Model([input_left, input_right], distance)

    # load weights into new model
    if path.exists(self.weights_file) and load_weights:
      model.load_weights(self.weights_file)
      print("# Loaded weights from disk")

    model.compile(loss=self.contrastive_loss,
                  # optimizer=optimizers.RMSprop(),
                  optimizer=optimizers.SGD(lr=learning_rate, momentum=0.9),
                  # optimizer=optimizers.Adam(learning_rate),
                  metrics=[self.accuracy])

    return model

  def train(self, train_dataset, val_dataset, epochs=50, batch_size=32, learning_rate=1e-3, overwrite_model=False):
    model = self.get_model(load_weights=not overwrite_model, learning_rate=learning_rate)
    # model.summary()

    # # Tensorboard callback
    # log_dir = "logs/fit/" + datetime.now().strftime("%Y%m%d-%H%M%S")
    # tensorboard_callback = callbacks.TensorBoard(log_dir=log_dir, histogram_freq=1)

    # train model
    model.fit(train_dataset.shuffle(buffer_size=1024).batch(batch_size),
              validation_data=val_dataset.batch(batch_size),
              # callbacks=[tensorboard_callback],
              epochs=epochs)

    # serialize model to JSON
    if not path.exists(self.model_file) or overwrite_model:
      with open(self.model_file, "w") as f:
        f.write(model.to_json())
        print("# Saved model to disk")

    # serialize weights to HDF5
    model.save_weights(self.weights_file)
    print("# Saved weights to disk")

    return model
