import os
from functools import reduce

import cv2
import numpy as np
import simplejson as json


class FacePreprocessor:
  def __init__(self, detector):
    self.detector = detector

  def apply(self, img, as_grayscale=True):
    face = self.detect_face(img)

    if face is None:
      # print("There are no face detected from the image.")
      return img, False

    # crop face from image
    img_res = self.crop(img, face["box"])

    # align face
    landmark = face["keypoints"]
    landmark["left_eye"] = (landmark["left_eye"][0] - face["box"][0],
                            landmark["left_eye"][1] - face["box"][1])
    landmark["right_eye"] = (landmark["right_eye"][0] - face["box"][0],
                             landmark["right_eye"][1] - face["box"][1])
    img_res = self.align_face(img_res, landmark, img_res.shape[:2])

    # convert image color to grayscale
    if as_grayscale:
      img_res = cv2.cvtColor(img_res, cv2.COLOR_RGB2GRAY)

    return img_res, True

  def detect_face(self, img, cache_file=None):
    def max_bbox(prev, item):
      prev_size = prev["box"][2] * prev["box"][3]
      item_size = item["box"][2] * item["box"][3]
      return prev if prev_size > item_size else item

    if cache_file != None:
      # read from cache file
      if os.path.exists(cache_file):
        with open(cache_file, "r") as f:
          faces = json.loads(f.read())
      else:
        faces = self.detector.detect_faces(img)

        with open(cache_file, "w") as f:
          f.write(json.dumps(faces))
    else:
      # detect faces from image
      faces = self.detector.detect_faces(img)

    # get largest face bbox from detected faces
    if len(faces) > 0:
      return reduce(max_bbox, faces)
    else:
      return None

  def align_face(self, image, landmarks, size, marker=False):
    leftEyeCenter = (landmarks['left_eye'][0], landmarks['left_eye'][1])
    rightEyeCenter = (landmarks['right_eye'][0], landmarks['right_eye'][1])

    # draw the circle at centers and line connecting to them
    if marker:
      cv2.circle(image, leftEyeCenter, 2, (255, 0, 0), 10)
      cv2.circle(image, rightEyeCenter, 2, (255, 0, 0), 10)
      cv2.line(image, leftEyeCenter, rightEyeCenter, (255, 0, 0), 10)

    # find and angle of line by using slop of the line.
    dY = landmarks['right_eye'][1] - landmarks['left_eye'][1]
    dX = landmarks['right_eye'][0] - landmarks['left_eye'][0]
    angle = np.degrees(np.arctan2(dY, dX))
    height, width = size

    # compute center (x, y)-coordinates (i.e., the median point)
    # between the two eyes in the input image
    eyes_center = ((landmarks['left_eye'][0] + landmarks['right_eye'][0]) // 2,
                   (landmarks['left_eye'][1] + landmarks['right_eye'][1]) // 2)

    # grab the rotation matrix for rotating and scaling the face
    M = cv2.getRotationMatrix2D(eyes_center, angle, 1)

    return cv2.warpAffine(image, M, (width, height), flags=cv2.INTER_CUBIC)

  def crop(self, img, bbox):
    x, y, width, height = bbox
    x = x if x > 0 else 0
    y = y if y > 0 else 0
    width = width if width > 0 else 0
    height = height if height > 0 else 0

    return np.array(img)[y:(y + height), x:(x + width)]
