from tensorflow.keras import layers, models

# __________________________________________________________________________________________________
# Layer (type)                    Output Shape         Param #     Connected to
# ==================================================================================================
# input_1 (InputLayer)            [(None, 31, 39, 1)]  0
# __________________________________________________________________________________________________
# conv2d (Conv2D)                 (None, 28, 36, 20)   340         input_1[0][0]
# __________________________________________________________________________________________________
# max_pooling2d (MaxPooling2D)    (None, 14, 18, 20)   0           conv2d[0][0]
# __________________________________________________________________________________________________
# dropout (Dropout)               (None, 14, 18, 20)   0           max_pooling2d[0][0]
# __________________________________________________________________________________________________
# conv2d_1 (Conv2D)               (None, 12, 16, 40)   7240        dropout[0][0]
# __________________________________________________________________________________________________
# max_pooling2d_1 (MaxPooling2D)  (None, 6, 8, 40)     0           conv2d_1[0][0]
# __________________________________________________________________________________________________
# dropout_1 (Dropout)             (None, 6, 8, 40)     0           max_pooling2d_1[0][0]
# __________________________________________________________________________________________________
# conv2d_2 (Conv2D)               (None, 4, 6, 60)     21660       dropout_1[0][0]
# __________________________________________________________________________________________________
# max_pooling2d_2 (MaxPooling2D)  (None, 2, 3, 60)     0           conv2d_2[0][0]
# __________________________________________________________________________________________________
# dropout_2 (Dropout)             (None, 2, 3, 60)     0           max_pooling2d_2[0][0]
# __________________________________________________________________________________________________
# conv2d_3 (Conv2D)               (None, 1, 2, 80)     19280       dropout_2[0][0]
# __________________________________________________________________________________________________
# flatten_1 (Flatten)             (None, 360)          0           dropout_2[0][0]
# __________________________________________________________________________________________________
# flatten (Flatten)               (None, 160)          0           conv2d_3[0][0]
# __________________________________________________________________________________________________
# dense_1 (Dense)                 (None, 160)          57760       flatten_1[0][0]
# __________________________________________________________________________________________________
# dense (Dense)                   (None, 160)          25760       flatten[0][0]
# __________________________________________________________________________________________________
# add (Add)                       (None, 160)          0           dense_1[0][0]
#                                                                  dense[0][0]
# __________________________________________________________________________________________________
# dense_2 (Dense)                 (None, 10)           1610        add[0][0]
# ==================================================================================================
# Total params: 133,650
# Trainable params: 133,650
# Non-trainable params: 0
# __________________________________________________________________________________________________


def deepid(classes, input_shape=(31, 39, 1)):
  inputs = layers.Input(shape=input_shape)
  conv1 = layers.Conv2D(20, kernel_size=(4, 4), activation="relu")(inputs)
  conv1 = layers.MaxPooling2D(pool_size=(2, 2), strides=2)(conv1)
  conv1 = layers.Dropout(.5)(conv1)

  conv2 = layers.Conv2D(40, kernel_size=(3, 3), activation="relu")(conv1)
  conv2 = layers.MaxPooling2D(pool_size=(2, 2), strides=2)(conv2)
  conv2 = layers.Dropout(.5)(conv2)

  conv3 = layers.Conv2D(60, kernel_size=(3, 3), activation="relu")(conv2)
  conv3 = layers.MaxPooling2D(pool_size=(2, 2), strides=2)(conv3)
  conv3 = layers.Dropout(.5)(conv3)

  conv4 = layers.Conv2D(80, kernel_size=(2, 2), activation="relu")(conv3)
  fc1 = layers.Flatten()(conv4)
  fc1 = layers.Dense(160, activation="relu")(fc1)

  fc2 = layers.Flatten()(conv3)
  fc2 = layers.Dense(160, activation="relu")(fc2)

  # Deep hidden identity features (DeepID)
  deepid = layers.add([fc1, fc2])

  # deepid = layers.Dense(classes, activation="softmax")(deepid)
  # deepid = layers.Dense(160, activation="sigmoid")(deepid)

  return deepid

## Change Last layer
# new_layer = layers.Dense(15, activation='softmax', name='my_dense')
# outputs = new_layer(model.layers[-2].output)
# model2 = models.Model(model.input, outputs)
# model2.summary()
