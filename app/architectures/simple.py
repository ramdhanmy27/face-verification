from tensorflow.keras import layers

def simple(inputs):
  x = layers.Conv2D(32, kernel_size=(4, 4), activation="relu")(inputs)
  x = layers.MaxPooling2D(pool_size=(3, 3))(x)

  x = layers.Conv2D(64, kernel_size=(3, 3), activation="relu")(x)
  x = layers.MaxPooling2D(pool_size=(2, 2))(x)

  x = layers.Flatten()(x)
  x = layers.Dense(128, activation="sigmoid")(x)

  return x
