from tensorflow.keras import layers, models

# _________________________________________________________________
# Layer (type)                 Output Shape              Param #
# =================================================================
# input_1 (InputLayer)         [(None, 46, 46, 1)]       0
# _________________________________________________________________
# conv2d (Conv2D)              (None, 41, 41, 5)         185
# _________________________________________________________________
# conv2d_1 (Conv2D)            (None, 36, 36, 14)        2534
# _________________________________________________________________
# flatten (Flatten)            (None, 18144)             0
# _________________________________________________________________
# dense (Dense)                (None, 60)                1088700
# _________________________________________________________________
# dense_1 (Dense)              (None, 40)                2440
# =================================================================
# Total params: 1,093,859
# Trainable params: 1,093,859
# Non-trainable params: 0
# _________________________________________________________________


def siamese(inputs):
  x = layers.Conv2D(5, kernel_size=(6, 6), activation="relu")(inputs)
  x = layers.Conv2D(14, kernel_size=(6, 6), activation="relu")(x)
  x = layers.Flatten()(x)
  x = layers.Dense(60, activation="relu")(x)
  x = layers.Dense(40, activation="relu")(x)

  return x

# inputs = layers.Input(shape=[46, 46, 1])
# outputs = siamese(inputs)

# model = models.Model(inputs, outputs)
# model.summary()
