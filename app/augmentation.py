from random import randrange

import cv2
import numpy as np


class Augmentation:
  def apply(self, image, im_size=None, crop_size=(160, 160)):
    image = self.random_crop(image, crop_size)

    if not im_size is None or crop_size == im_size:
      image = self.resize(image, im_size)

    image = self.normalize(image)

    return image

  def random_crop(self, image, crop_size=(160, 160)):
    h, w = image.shape
    crop_w, crop_h = crop_size
    left_margin = randrange(0, w - crop_w)
    top_margin = randrange(0, h - crop_h)
    right = left_margin + crop_w
    bottom = top_margin + crop_h

    return image[top_margin:bottom, left_margin:right]

  def normalize(self, image):
    return image.astype("float32") / 255

  def resize(self, image, size):
    return cv2.resize(image, (size[0], size[1]))

  def grayscale(self, image):
    return cv2.cvtColor(image, cv2.COLOR_RGB2GRAY)