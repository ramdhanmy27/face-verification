import os
import random
import re
import sys
from glob import glob
from os import path
from random import randrange

import cv2
import numpy as np
import tensorflow as tf
from PIL import Image

from app import Augmentation, FacePreprocessor


class Dataset:
  def __init__(self, images_path):
    self.augmentation = Augmentation()
    self.images_path = images_path

  # TODO: determine fixed image size
  def from_tfrecords(self, path, split_train=0.7, im_shape=[72, 72, 3], im_resize=None, grayscale=False):
    self.im_shape = im_shape
    self.im_resize = im_resize
    self.grayscale = grayscale

    record_files = glob(path)
    random.shuffle(record_files)

    split_val = 0.1
    total_records = len(record_files)
    train_len = round(total_records * split_train)
    val_len = round(total_records * split_val)

    train_records = record_files[:train_len]
    train_dataset = tf.data.TFRecordDataset(train_records).map(self.parse_record)

    val_records = record_files[train_len:(train_len + val_len)]
    val_dataset = tf.data.TFRecordDataset(val_records).map(self.parse_record)

    test_records = record_files[(train_len + val_len):]
    test_dataset = tf.data.TFRecordDataset(test_records).map(self.parse_record)

    return train_dataset, val_dataset, test_dataset

  # def from_generator(self, path, split_train=0.7, input_shape=(160, 160, 1)):

  @tf.function
  def parse_record(self, example):
    tf_example = tf.io.parse_single_example(example, {
        "left": tf.io.FixedLenFeature([], tf.string),
        "right": tf.io.FixedLenFeature([], tf.string),
        "similarity": tf.io.FixedLenFeature([], tf.int64),
    })

    left = tf.reshape(tf.io.decode_raw(tf_example["left"], out_type=tf.uint8), self.im_shape)
    right = tf.reshape(tf.io.decode_raw(tf_example["right"], out_type=tf.uint8), self.im_shape)

    # image resize
    if self.im_resize != None:
      left = tf.image.resize(left, self.im_resize)
      right = tf.image.resize(right, self.im_resize)

    # convert to grayscale
    if self.grayscale:
      left = tf.image.rgb_to_grayscale(left)
      right = tf.image.rgb_to_grayscale(right)

    # normalize image
    left = tf.cast(left, tf.float32) / 255
    right = tf.cast(right, tf.float32) / 255

    # TODO: fix similarity label
    similarity = 0 if tf_example["similarity"] == 1 else 1  # swap 0 <-> 1
    # similarity = tf_example["similarity"]

    return {"input_left": left, "input_right": right}, similarity

  def __serialize_pair(self, left_img, right_img, similarity):
    tf_example = tf.train.Example(features=tf.train.Features(feature={
        "left": tf.train.Feature(bytes_list=tf.train.BytesList(value=[left_img.tobytes()])),
        "right": tf.train.Feature(bytes_list=tf.train.BytesList(value=[right_img.tobytes()])),
        "similarity": tf.train.Feature(int64_list=tf.train.Int64List(value=[similarity])),
    }))

    return tf_example.SerializeToString()

  # TODO: determine fixed image size
  def prepare(self, record_files="./dataset/data-%06d.tfrecord", batch_size=2000, im_size=(72, 72)):
    split_num = 0
    count = 0
    batch_data = []
    identities = np.array([identity for identity in os.listdir(self.images_path)
                           if path.isdir(path.join(self.images_path, identity))])

    for identity in identities:
      img_dir = path.join(self.images_path, identity)
      img_names = np.array(os.listdir(img_dir))

      for img_name in img_names:
        img_file = path.join(img_dir, img_name)
        left_img = self.augmentation.apply(Image.open(img_file), im_size)

        # make positive pair
        pair_img_name = random.choice(
            img_names[np.where(img_names != img_name)])
        pair_img = Image.open(path.join(img_dir, pair_img_name))
        pair_img = self.augmentation.apply(pair_img, im_size)
        batch_data.append(self.__serialize_pair(left_img, pair_img, 1))

        # make negative pair
        pair_identity = random.choice(
            identities[np.where(identities != identity)])
        pair_img_dir = path.join(self.images_path, pair_identity)
        pair_img_name = random.choice(os.listdir(pair_img_dir))
        pair_img = Image.open(path.join(pair_img_dir, pair_img_name))
        pair_img = self.augmentation.apply(pair_img, im_size)
        batch_data.append(self.__serialize_pair(left_img, pair_img, 0))

        count += 1

        # split tfrecord if count reach batch_size limit
        if count % batch_size == 0:
          with tf.io.TFRecordWriter(record_files % split_num) as writer:
            random.shuffle(batch_data)

            for serialized_data in batch_data:
              writer.write(serialized_data)

          batch_data = []
          split_num += 1
